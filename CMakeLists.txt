cmake_minimum_required(VERSION 3.12)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# initialize the SDK based on PICO_SDK_PATH
# note: this must happen before project()
include(pico_sdk_import.cmake)

project(pi_pico_sdd1306)

# initialize the Pico SDK
pico_sdk_init()

# rest of your project

add_executable(sdd1306_test
    src/main.cpp
)
target_compile_options(sdd1306_test PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-std=c++20>)
target_include_directories(sdd1306_test PRIVATE lib)

target_link_libraries(sdd1306_test 
    pico_stdlib
    pico_bootrom
    hardware_i2c
    )

pico_enable_stdio_usb(sdd1306_test 1)
pico_enable_stdio_uart(sdd1306_test 0)
pico_add_extra_outputs(sdd1306_test)


add_executable(lcd_1602_i2c
    src/lcd_1502_i2c.cpp
    )
target_link_libraries(lcd_1602_i2c pico_stdlib hardware_i2c)
pico_add_extra_outputs(lcd_1602_i2c)

